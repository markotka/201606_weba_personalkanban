<?php

$_REPOSITORY_FOLDER = "/Users/markot/workflow_test/"; // MUST end with slash





function moveItem($item, $sourceList, $targetList){
	global $_REPOSITORY_FOLDER;
	$ans = array();

	$src = $_REPOSITORY_FOLDER . $sourceList . "/".$item. "/";
	$trgt = $_REPOSITORY_FOLDER . $targetList . "/".$item. "/";

	if (!rename($src,$trgt)) {
		$ans[] = "failed";
	}
	else{
		$ans[] = "ok";
	}
	
	echo json_encode($ans);
}



function getItemsInList($listname){
	global $_REPOSITORY_FOLDER;
	$listOfItems = array();
	$folder = $_REPOSITORY_FOLDER . $listname;	
	if ($handle = opendir($folder)) {
		chdir($folder);
		while (false !== ($entry = readdir($handle))) {
			if (is_dir($entry)){
				if ($entry != "." & $entry != ".."){
					$listOfItems[] = $entry;
				}
			}
		}
		closedir($handle);
	}
	echo json_encode($listOfItems);
}



function getListOfLists(){
	global $_REPOSITORY_FOLDER;
	$listOfLists = array();
	$folder = $_REPOSITORY_FOLDER;	// MUST end with slash
	if ($handle = opendir($folder)) {
		chdir($folder);
		while (false !== ($entry = readdir($handle))) {
			if (is_dir($entry)){
				if ($entry != "." & $entry != ".."){
					$listOfLists[] = $entry;
				}
			}
		}
		closedir($handle);
	}
	echo json_encode($listOfLists);
}








if (isset($_REQUEST["function"])){
	$func = $_REQUEST["function"];
	switch ($func){
		case "getListOfLists":
			getListOfLists();
			break;
		case "getItemsInList":
			getItemsInList($_REQUEST["listname"]);
			break;
		case "moveItem":
			moveItem($_REQUEST["item"],$_REQUEST["sourceList"],$_REQUEST["targetList"]);
			break;
	}
}





