<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="kanban.css">
		
		<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
		<script src="http://code.jquery.com/jquery-1.12.0.js"></script>
	</head>
	
	<body>
	
		<div id="container_of_lists" class="container_of_lists"></div>
		
		<script type="text/javascript">

			//
			var listOfAllLists;

			
			//
			// DRAG-AND-DROP HANDLERS
			//
		
			function allowDrop(ev) {
			    ev.preventDefault();
			}
			
			function drag(ev,el) {
				//alert(el.parentElement.getAttribute("id"));
			    ev.dataTransfer.setData("item", ev.target.id);
			    ev.dataTransfer.setData("sourceList",el.parentElement.getAttribute("id"));
			    ev.dropEffect = "move";
			}
			
			function drop(ev,el) {
			    ev.preventDefault();
			    var item = ev.dataTransfer.getData("item");
			    var sourceList = ev.dataTransfer.getData("sourceList");
			    var targetList = el.id;
			    if (ev.target.class == "container_of_lists"){
			        alert("container_of_lists");
			    }

			    // call API to move folder and move in DOM if successful
			    var url = "http://localhost/kanban/kanban_api.php?function=moveItem&item="+item+"&sourceList="+sourceList+"&targetList="+targetList;
				$.getJSON(url, function(api_ret) {
					alert(api_ret);
					if (api_ret == "ok"){
						el.appendChild(document.getElementById(item));
					}
				});

			}



			//
			// OTHER FUNCTIONS
			//
						
			function getListHtml(listName){
				var n = listName.replace(/_/gi, " ");
				var html = "<div ondrop='drop(event,this)' ondragover='allowDrop(event)' class='kanban_list' id='"+listName+"'><h3>"+n+"</h3></div>";
				return html;
			}
			
			function getItemHtml(listName, itemName){
				var n = itemName.replace(/_/gi, " "); 
				var html = "<div draggable='true' ondragstart='drag(event,this)' class='kanban_item' id='"+itemName+"'><h4>"+n+"</h4></div>";
				
				return html;
			}
			
			
			
			
			
			
			
			function paintList(listname,listOfItems){
				$.each(listOfItems, function(k,v){
					$("#"+listname).append(getItemHtml(listname,v));
				});
			}
			
			function getItemsInList(listname){
				var url = "http://localhost/kanban/kanban_api.php?function=getItemsInList&listname=" + listname;
				$.getJSON(url, function(api_ret) {
					paintList(listname,api_ret)
				});
			}

			function paintAllLists(){
				$.each(listOfAllLists, function(k,v){
					$("#container_of_lists").append(getListHtml(v));
					getItemsInList(v);
				});
			}

			
			/**
			* Starting function
			*
			**/
			function getAllLists(){
				var url = "http://localhost/kanban/kanban_api.php?function=getListOfLists";
				$.getJSON(url, function(api_ret) {
					listOfAllLists = api_ret;
					paintAllLists();
				});
			
			}

			// START
			a = getAllLists();

		
		</script>
	</body>
</html>












